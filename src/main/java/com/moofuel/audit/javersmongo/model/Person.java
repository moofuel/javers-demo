package com.moofuel.audit.javersmongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.javers.core.metamodel.annotation.DiffIgnore;
import org.javers.core.metamodel.annotation.Entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "persons")
public class Person {

    @Id
    private ObjectId id;

    @DiffIgnore // данное поле не будет участвовать в изменениях
    private String name;
    private Integer age;
    private Set<String> qualities;

    /**
     * Javers хочет именно @Id над геттером
     */
    @org.javers.core.metamodel.annotation.Id
    public ObjectId getId() {
        return id;
    }
}

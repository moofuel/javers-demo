package com.moofuel.audit.javersmongo.controllers;

import com.moofuel.audit.javersmongo.model.Person;
import com.moofuel.audit.javersmongo.services.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Slf4j
@RestController
@RequestMapping("/persons")
public class PersonController {

    private PersonService personService;

    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping
    public Person createNew(@RequestBody Person person) {
        log.info("New Person incooooming!");
        return this.personService.createNew(person);
    }

    @PatchMapping
    public Person updateExisting(@RequestBody Person person) {
        log.info("Old Person gonna be chaaaanged!");
        return this.personService.updateExisting(person);
    }
}

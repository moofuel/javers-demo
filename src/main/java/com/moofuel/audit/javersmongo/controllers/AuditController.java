package com.moofuel.audit.javersmongo.controllers;

import com.moofuel.audit.javersmongo.model.Person;
import com.moofuel.audit.javersmongo.services.AuditService;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@RestController
@RequestMapping("/audit")
public class AuditController {

    private AuditService auditService;

    public AuditController(AuditService auditService) {
        this.auditService = auditService;
    }

    @GetMapping("/persons/{id}/changes")
    public String findChangesById(@PathVariable ObjectId id) {
        return this.auditService.findChangesById(id, Person.class);
    }

    @GetMapping("/persons/{id}/shadows")
    public String findShadowsById(@PathVariable ObjectId id) {
        return this.auditService.findShadowsById(id, Person.class);
    }

    @GetMapping("/persons/{id}/snapshots")
    public String findSnapshotsById(@PathVariable ObjectId id) {
        return this.auditService.findSnapshotsById(id, Person.class);
    }
}

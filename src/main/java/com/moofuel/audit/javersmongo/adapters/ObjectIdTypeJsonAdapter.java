package com.moofuel.audit.javersmongo.adapters;

import org.bson.types.ObjectId;
import org.javers.core.json.BasicStringTypeAdapter;
import org.springframework.stereotype.Component;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Component
public class ObjectIdTypeJsonAdapter extends BasicStringTypeAdapter {

    @Override
    public String serialize(Object sourceValue) {
        return sourceValue.toString();
    }

    @Override
    public Object deserialize(String serializedValue) {
        return new ObjectId(serializedValue);
    }

    @Override
    public Class getValueType() {
        return ObjectId.class;
    }
}

package com.moofuel.audit.javersmongo.services.impl;

import com.moofuel.audit.javersmongo.services.AuditService;
import lombok.extern.slf4j.Slf4j;
import org.bson.types.ObjectId;
import org.javers.core.Javers;
import org.javers.core.diff.Change;
import org.javers.core.metamodel.object.CdoSnapshot;
import org.javers.repository.jql.JqlQuery;
import org.javers.repository.jql.QueryBuilder;
import org.javers.shadow.Shadow;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Data history views
 * Data history can be fetched from JaversRepository using javers.find*() methods in one of three views:
 * Shadows, Changes, and Snapshots.
 * <p>
 * Shadow is a historical version of a domain object restored from a snapshot.
 * <p>
 * Change represents an atomic difference between two objects.
 * <p>
 * Snapshot is a historical state of a domain object captured as the property:value map
 * <p>
 * <a href="https://javers.org/documentation/jql-examples/">Javers JQL Documentation</a>
 *
 * @author Дмитрий
 * @since 15.11.2017
 */
@Slf4j
@Service
public class AuditServiceImpl implements AuditService {

    private Javers javers;

    public AuditServiceImpl(Javers javers) {
        this.javers = javers;
    }

    /**
     * Метод вернет все изменения сущности класса entityClass и с идентификатором id
     * Изменения(changes) - набор диффов от коммита к коммиту
     */
    @Override
    public String findChangesById(ObjectId id, Class entityClass) {
        final JqlQuery query = QueryBuilder.
                byInstanceId(id, entityClass).
                build();
        final List<Change> changes = javers.findChanges(query);
        return javers.getJsonConverter().toJson(changes);
    }

    /**
     * Метод вернет все исторические версии сущности(восстановленные из снэпшота)
     * класса entityClass, с идентификатором id и только если изменилось
     * значение поля "qualities"
     * Исторические версии(shadows) - кусок снэпшота из БД
     */
    @Override
    public String findShadowsById(ObjectId id, Class entityClass) {
        final JqlQuery query = QueryBuilder.
                byInstanceId(id, entityClass).
                withChangedProperty("qualities").
                build();
        final List<Shadow<Object>> shadows = javers.findShadows(query);
        return javers.getJsonConverter().toJson(shadows);
    }

    /**
     * Метод вернет все исторические состояния сущности класса entityClass,
     * с идентификатором id и только если изменилось значение поля "qualities"
     * Историческое состояние(Snapshots) - вся сущность из коллекции jv_snapshots
     */
    @Override
    public String findSnapshotsById(ObjectId id, Class entityClass) {
        final JqlQuery query = QueryBuilder.
                byInstanceId(id, entityClass).
                withChangedProperty("qualities").
                build();
        final List<CdoSnapshot> snapshots = javers.findSnapshots(query);
        return javers.getJsonConverter().toJson(snapshots);
    }
}

package com.moofuel.audit.javersmongo.services.impl;

import com.moofuel.audit.javersmongo.model.Person;
import com.moofuel.audit.javersmongo.repositories.PersonRepository;
import com.moofuel.audit.javersmongo.services.PersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Slf4j
@Service
public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person createNew(Person person) {
        log.info("Creating new {}", person);
        return this.personRepository.save(person);
    }

    @Override
    public Person updateExisting(Person person) {
        log.info("Updating existing Person with ID {} with {}", person.getId(), person);
        return this.personRepository.save(person);
    }
}

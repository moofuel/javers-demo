package com.moofuel.audit.javersmongo.services;

import org.bson.types.ObjectId;

/**
 * @author Дмитрий
 * @since 16.11.2017
 */
public interface AuditService {
    String findChangesById(ObjectId id, Class entityClass);

    String findShadowsById(ObjectId id, Class entityClass);

    String findSnapshotsById(ObjectId id, Class entityClass);
}

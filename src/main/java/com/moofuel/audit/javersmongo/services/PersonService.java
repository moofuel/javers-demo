package com.moofuel.audit.javersmongo.services;

import com.moofuel.audit.javersmongo.model.Person;

/**
 * @author Дмитрий
 * @since 16.11.2017
 */
public interface PersonService {
    Person createNew(Person person);

    Person updateExisting(Person person);
}

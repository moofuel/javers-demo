package com.moofuel.audit.javersmongo.repositories;

import com.moofuel.audit.javersmongo.model.Person;
import org.bson.types.ObjectId;
import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@JaversSpringDataAuditable
public interface PersonRepository extends MongoRepository<Person, ObjectId> {
}

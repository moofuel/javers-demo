package com.moofuel.audit.javersmongo.configurations;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.moofuel.audit.javersmongo.adapters.ObjectIdTypeJsonAdapter;
import lombok.extern.slf4j.Slf4j;
import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.MappingStyle;
import org.javers.core.diff.ListCompareAlgorithm;
import org.javers.core.json.JsonTypeAdapter;
import org.javers.java8support.Java8TypeAdapters;
import org.javers.repository.api.JaversRepository;
import org.javers.repository.mongo.MongoRepository;
import org.javers.spring.boot.mongo.JaversProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Slf4j
@Configuration
public class JaversConfig {

    @Autowired
    private JaversProperties javersProperties;

    @Autowired
    private MongoClient mongoClient; //from spring-boot-starter-data-mongodb

    @Autowired
    private MongoProperties mongoProperties;

    @Bean
    public Javers javers(ObjectIdTypeJsonAdapter adapter) {
        // copy-paste from JaversMongoAutoConfiguration.java

        log.info("Starting javers-spring-boot-starter-mongo ...");
        final MongoDatabase mongoDatabase = mongoClient.getDatabase(mongoProperties.getMongoClientDatabase());
        log.info("connecting to database: {}", mongoProperties.getMongoClientDatabase());
        final JaversRepository javersRepository = new MongoRepository(mongoDatabase);

        // end copy-paste
        final JaversBuilder javersBuilder = JaversBuilder.javers()
                .withListCompareAlgorithm(ListCompareAlgorithm.valueOf(javersProperties.getAlgorithm().toUpperCase()))
                .withMappingStyle(MappingStyle.valueOf(javersProperties.getMappingStyle().toUpperCase()))
                .withNewObjectsSnapshot(javersProperties.isNewObjectSnapshot())
                .registerValueTypeAdapter(adapter)
                .withPrettyPrint(javersProperties.isPrettyPrint())
                .withTypeSafeValues(javersProperties.isTypeSafeValues())
                .registerJaversRepository(javersRepository)
                .withPackagesToScan(javersProperties.getPackagesToScan());
        for (JsonTypeAdapter jsonTypeAdapter : Java8TypeAdapters.adapters()) {
            javersBuilder.registerValueTypeAdapter(jsonTypeAdapter);
        }
        return javersBuilder.build();

    }
}

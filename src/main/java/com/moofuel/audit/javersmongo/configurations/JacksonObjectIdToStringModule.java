package com.moofuel.audit.javersmongo.configurations;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;

/**
 * @author Дмитрий
 * @since 15.11.2017
 */
@Component
public class JacksonObjectIdToStringModule extends SimpleModule {

    public JacksonObjectIdToStringModule() {
        super("JacksonObjectIdToStringModule", Version.unknownVersion());
    }

    @Override
    public void setupModule(SetupContext context) {
        SimpleSerializers simpleSerializers = new SimpleSerializers();
        simpleSerializers.addSerializer(ObjectId.class, new ToStringSerializer());
        context.addSerializers(simpleSerializers);
    }
}
